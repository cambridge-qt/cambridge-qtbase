QT.concurrent.VERSION = 5.0.0
QT.concurrent.MAJOR_VERSION = 5
QT.concurrent.MINOR_VERSION = 0
QT.concurrent.PATCH_VERSION = 0

QT.concurrent.name = QtConcurrent
QT.concurrent.bins = $$QT_MODULE_BIN_BASE
QT.concurrent.includes = $$QT_MODULE_INCLUDE_BASE/QtConcurrent
QT.concurrent.private_includes = $$QT_MODULE_INCLUDE_BASE/QtConcurrent/$$QT.concurrent.VERSION
QT.concurrent.sources = $$QT_MODULE_BASE/src/concurrent
QT.concurrent.libs = $$QT_MODULE_LIB_BASE
QT.concurrent.plugins = $$QT_MODULE_PLUGIN_BASE
QT.concurrent.imports = $$QT_MODULE_IMPORT_BASE
QT.concurrent.depends = core
QT.concurrent.DEFINES = QT_CONCURRENT_LIB
