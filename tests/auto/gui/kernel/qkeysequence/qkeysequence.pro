CONFIG += testcase
CONFIG += parallel_test
TARGET = tst_qkeysequence

QT += testlib
QT += core-private gui-private

SOURCES  += tst_qkeysequence.cpp

RESOURCES += qkeysequence.qrc
