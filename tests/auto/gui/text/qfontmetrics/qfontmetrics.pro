CONFIG += testcase
CONFIG += parallel_test
TARGET = tst_qfontmetrics
QT += testlib
SOURCES  += tst_qfontmetrics.cpp
RESOURCES += testfont.qrc
