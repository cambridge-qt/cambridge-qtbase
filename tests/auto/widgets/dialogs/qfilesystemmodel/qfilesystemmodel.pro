CONFIG += testcase
CONFIG += parallel_test

QT += widgets widgets-private
QT += core-private gui testlib

SOURCES		+= tst_qfilesystemmodel.cpp
TARGET		= tst_qfilesystemmodel
